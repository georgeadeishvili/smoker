package io.rugi.smoker.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import com.google.firebase.auth.FirebaseAuth
import io.rugi.smoker.R

class LoginFragment: Fragment(R.layout.fragment_login) {
    private lateinit var loginBtn: Button
    private lateinit var goToRegistrationBtn: Button
    private lateinit var emailField: EditText
    private lateinit var passwordField: EditText
    private lateinit var navController: NavController
    private lateinit var firebaseAuth: FirebaseAuth

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        firebaseAuth = FirebaseAuth.getInstance()

        loginBtn = view.findViewById(R.id.loginBtn)
        goToRegistrationBtn = view.findViewById(R.id.goToRegistration)
        emailField = view.findViewById(R.id.editTextTextEmailAddress)
        passwordField = view.findViewById(R.id.editTextTextPassword)

        loginBtn.setOnClickListener {
            firebaseAuth.signInWithEmailAndPassword(emailField.text.toString(), passwordField.text.toString())
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        navController.navigate(R.id.action_loginFragment_to_tabNavigation)
                    }
                }
        }

        goToRegistrationBtn.setOnClickListener {
            navController.navigate(R.id.action_loginFragment_to_registrationFragment)
        }
    }

}