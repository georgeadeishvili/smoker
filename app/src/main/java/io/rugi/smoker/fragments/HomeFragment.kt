package io.rugi.smoker.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import io.rugi.smoker.R
import kotlin.random.Random

class HomeFragment: Fragment(R.layout.fragment_home) {
    private lateinit var mistakeBtn: Button
    private lateinit var mistakeText: TextView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val options = arrayOf(
            "set funny sound as phone ringtone for a week",
            "Shave your head, in case of baldness shave beard",
            "Express yourself in love with her/him",
            "for Girls = Grow hair on the legs or arms. for Boys - Shave hair on the legs or arms",
            "Invite friend in Restaurant or Sakhinkle"
        )

        mistakeBtn = view.findViewById(R.id.mistakeBtn)
        mistakeText = view.findViewById(R.id.mistakeText)

        mistakeBtn.setOnClickListener {
            mistakeText.text = options[Random.nextInt(0, 5)]
        }
    }
}