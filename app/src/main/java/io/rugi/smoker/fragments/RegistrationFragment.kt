package io.rugi.smoker.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.google.firebase.auth.FirebaseAuth
import io.rugi.smoker.R

class RegistrationFragment: Fragment(R.layout.fragment_registration) {
    private lateinit var navController: NavController

    private lateinit var fireBaseAuth: FirebaseAuth

    private lateinit var registrationBtn: Button
    private lateinit var goToLoginScreenBtn: Button
    private lateinit var firstNameField: EditText
    private lateinit var lastNameField: EditText
    private lateinit var emailField: EditText
    private lateinit var passwordField: EditText
    private lateinit var confirmPasswordField: EditText

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        fireBaseAuth = FirebaseAuth.getInstance()

        registrationBtn = view.findViewById(R.id.registrationBtn)
        goToLoginScreenBtn = view.findViewById(R.id.goToLogin)
        firstNameField = view.findViewById(R.id.editTextTextPersonName)
        lastNameField = view.findViewById(R.id.editTextTextPersonName2)
        emailField = view.findViewById(R.id.editTextTextEmailAddress2)
        passwordField = view.findViewById(R.id.editTextTextPassword2)
        confirmPasswordField = view.findViewById(R.id.editTextTextPassword3)

        registrationBtn.setOnClickListener {

            if (passwordField.text.toString() == confirmPasswordField.text.toString()) {
                fireBaseAuth.createUserWithEmailAndPassword(emailField.text.toString(), passwordField.text.toString())
                    .addOnCompleteListener {
                        task -> if (task.isSuccessful) {
                            navController.navigate(R.id.action_registrationFragment_to_tabNavigation)
                        }
                    }
            }
        }

        goToLoginScreenBtn.setOnClickListener {
            navController.navigate(R.id.action_registrationFragment_to_loginFragment)
        }
    }
}